(define-module (nano-channel packages emacs-xyz)
  #:use-module (gnu)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build utils)
  #:use-module (guix build-system)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix git-download)
  #:use-module (gnu packages texinfo)
  #:use-module (guix download))

;;; Mexican holidays
(define-public emacs-mexican-holidays
  (package
    (name "emacs-mexican-holidays")
    (version "20210604.1421")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://melpa.org/packages/mexican-holidays-"
             version
             ".el"))
       (sha256
        (base32 "0qm6kb6hc6y9izdmnvp73bx2m6w3c5rz084r8y8ycjxa0bq4wh9d"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/sggutier/mexican-holidays")
    (synopsis "Mexico holidays for Emacs calendar.")
    (description
     "
Remove default holidays, then append mexican calendar

(customize-set-variable 'holiday-bahai-holidays nil)
(customize-set-variable 'holiday-hebrew-holidays nil)
(customize-set-variable 'holiday-islamic-holidays nil)
(setq calendar-holidays (append calendar-holidays holiday-mexican-holidays))
")
    (license #f)))


;;; Consult

(define-public emacs-consult-dir
  (package
    (name "emacs-consult-dir")
    (version "20221001.1748")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/karthink/consult-dir.git")
                    (commit "ed8f0874d26f10f5c5b181ab9f2cf4107df8a0eb")))
              (sha256
               (base32
                "0fkd4ybgh06x1ci4wqpcyvc3vw97v0qc5lzlyj9ycfgdqixym58y"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-consult emacs-project))
    (home-page "https://github.com/karthink/consult-dir")
    (synopsis "Insert paths into the minibuffer prompt")
    (description
     "Consult-dir implements commands to easily switch between \"active\" directories.
The directory candidates are collected from user bookmarks, projectile project
roots (if available), project.el project roots and recentf file locations.  The
`default-directory variable not changed in the process.  Call `consult-dir from
the minibuffer to choose a directory with completion and insert it into the
minibuffer prompt, shadowing or deleting any existing directory.  The file name
input is retained.  This lets the user switch to distant directories very
quickly when finding files, for instance.  Call `consult-dir from a regular
buffer to choose a directory with completion and then interactively find a file
in that directory.  The command run with this directory is configurable via
`consult-dir-default-command and defaults to `find-file'.  Call
`consult-dir-jump-file from the minibuffer to asynchronously find a file
anywhere under the directory that is currently in the prompt.  This can be used
with `consult-dir to quickly switch directories and find files at an arbitrary
depth under them. `consult-dir-jump-file uses `consult-find under the hood.  To
use this package, bind `consult-dir and `consult-dir-jump-file under the
`minibuffer-local-completion-map or equivalent, and `consult-dir to the global
map. (define-key minibuffer-local-completion-map (kbd \"C-x C-d\") #'consult-dir)
(define-key minibuffer-local-completion-map (kbd \"C-x C-j\")
#'consult-dir-jump-file) (define-key global-map (kbd \"C-x C-d\") #'consult-dir)
Directory sources configuration: - To make recent directories available, turn on
`recentf-mode'. - To make projectile projects available, turn on projectile-mode
and configure `consult-dir-project-list-function'.  Note that Projectile is NOT
required to install this package. - To make project.el projects available,
configure `consult-dir-project-list-function'.  To change directory sources or
their ordering, customize `consult-dir-sources'.")
    (license #f)))


(define-public emacs-consult-eglot
  (package
    (name "emacs-consult-eglot")
    (version "20220409.1238")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mohkale/consult-eglot.git")
                    (commit "0da8801dd8435160ce1f62ad8066bd52e38f5cbd")))
              (sha256
               (base32
                "1qxk1npxbf8m3g9spikgdxcf6mzjx6cwy3f5vn6zz5ksh14xw3sd"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-eglot emacs-consult emacs-project))
    (home-page "https://github.com/mohkale/consult-eglot")
    (synopsis "A consulting-read interface for eglot")
    (description
     "Query workspace symbol from eglot using consult.

This package provides a single command `consult-eglot-symbols' that uses the lsp
workspace/symbol procedure to get a list of symbols exposed in the current
workspace.  This differs from the default document/symbols call, that eglot
exposes through imenu, in that it can present symbols from multiple open files
or even files not indirectly loaded by an open file but still used by your
project.

This code was partially adapted from the excellent consult-lsp package.")
    (license #f)))

(define-public emacs-consult-project-extra
  (package
    (name "emacs-consult-project-extra")
    (version "20221013.1014")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Qkessler/consult-project-extra.git")
                    (commit "9fdf45fa40471900b0b158d73c4b1521a13d47ef")))
              (sha256
               (base32
                "02m5vslf46kc9qjdyjqg3kjgv7a8vs0vmmc9gjh62nxfnz1dl7gn"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-consult emacs-project))
    (home-page "https://github.com/Qkessler/consult-project-extra")
    (synopsis "Consult integration for project.el")
    (description
     "This package creates an endpoint for accessing different project sources.  The
consult view can be narrowed to: (b) current project's buffers, (f) current
project's files and (p) to select from the list of known projects.  The buffer
and project file sources are only enabled in case that the user is in a project
file/buffer.  See `project-current'.  A different action is issued depending on
the source.  For both buffers and project files, the default action is to visit
the selected element.  When a known project is selected, a list to select from
is created with the selected project's files.")
    (license #f)))


(define-public emacs-consult-recoll
  (package
    (name "emacs-consult-recoll")
    (version "0.8")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://elpa.gnu.org/packages/consult-recoll-" version
                    ".tar"))
              (sha256
               (base32
                "02vg1rr2fkcqrrivqgggdjdq0ywvlyzazwq1xd02yah3j4sbv4ag"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-consult))
    (home-page "https://codeberg.org/jao/consult-recoll")
    (synopsis "Recoll queries using consult")
    (description
     "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 		 RECOLL QUERIES IN EMACS USING CONSULT
		━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ Table of Contents ───────────────── 1.
 About ..  1.  Tip: using consult-recoll with helm 2.  Searching ..  1.  Tip:
Two-level filtering 3.  Displaying results ..  1.  Example: formatting results
list ..  2.  Integration with embark-collect ..  3.  Tip: displaying snippets in
results list ..  4.  Tip: disabling mime type groups 4.  Opening search results
..  1.  Example: opening PDFs with external viewer ..  2.  Example: Opening
emails with notmuch 5.  Thanks 1 About ═══════ [Recoll] is a local search engine
that knows how to index a wide variety of file formats, including PDFs, org and
other text files and emails.  It also offers a sophisticated query language,
and, for some document kinds, snippets in the the found documents actually
matching the query at hand.  This package provides an emacs interface to perform
recoll queries, and display its results, via [consult].  It is also recommened
that you use a a package for vertical display of completions that works well
with /consult/, such as [vertico].
<https://codeberg.org/jao/consult-recoll/raw/branch/main/consult-recoll.png>
This package is part of [GNU ELPA], so for recent Emacs versions you can install
it directly via `M-x package-install RET consult-recoll RET'. [Recoll]
<https://www.lesbonscomptes.com/recoll/> [consult]
<http://elpa.gnu.org/packages/consult.html> [vertico]
<http://elpa.gnu.org/packages/vertico.html> [GNU ELPA]
<http://elpa.gnu.org/packages/consult-recoll.html> 1.1 Tip: using consult-recoll
with helm ─────────────────────────────────────── If you use `helm-mode', you'll
need to disable helm's completing read for `consult-recoll', with something
like: ┌──── │ (with-eval-after-load \"helm\" │ (with-eval-after-load \"recoll\" │
(add-to-list helm-completing-read-handlers-alist │ 		 (cons #'consult-recoll
nil)))) └──── 2 Searching ═══════════ The entry point of `consult-recoll is the
interactive command `consult-recoll'.  Just invoke it (e.g., via `M-x
consult-recoll') to perform any query and get its results dynamically displayed
in the minibuffer, with \"live\" updates as the query changes.  Selecting any of
the candidate results will open the associated file, using the functions in
`consult-recoll-open-fns (see [Opening search results] below).  By default, your
input will be interpreted as a recoll query, in the recoll query language (so
you can issue queries like \"author:jao@@foo.io\" or \"dir:/home/jao/docs
mime:application/pdf where is wally\", and so on).  You can fine tune how queries
are issued by customizing `consult-recoll-search-flags'. [Opening search
results] See section 4 2.1 Tip: Two-level filtering ────────────────────────────
`consult-recoll builds on the asychronous logic inside `consult.el', so you can
use consult's handy [two-level filtering], which allows searching over the
results of a query.  For example, if you start typing ┌──── │ #goedel's theorem
└──── see a bunch of results, and want to narrow them to those lines matching,
say, \"hofstadter\", you can type `# (which stops further recoll queries) followed
by the term you're interested in: ┌──── │ #goedel's theorem#hofstadter └──── at
which point only matches containing \"hofstadter\" will be offered. [two-level
filtering] <https://github.com/minad/consult#asynchronous-search> 3 Displaying
results ════════════════════ For each matching result, `consult-recoll retrieves
its title, full file name and mime type, and shows, by default, a line with the
first two in the minibuffer, using the customizable faces
`consult-recoll-title-face and `consult-recoll-url-face'.  You can provide your
own formatting function (perhaps stripping common prefixes of the file name, or
displaying also the MIME) as the value of the customizable variable
`consult-recoll-format-candidate'.  By default, `consult-recoll uses consult's
[live previews] to show, for each selected candidate hit, a buffer with further
information, including snippets of the file (when provided by recoll).  The
title, path and mime type of the document are also shown in previews.  See
[Opening search results] below for ways of customizing how Emacs will open
selected results. [live previews]
<https://github.com/minad/consult#live-previews> [Opening search results] See
section 4 3.1 Example: formatting results list
──────────────────────────────────── As mentioned, one can use
`consult-recoll-format-candidate to customize how search results are shown in
the minibufer.  For instance, i like to shorten paths removing common prefixes
and to show MIME types, so i use a formatter similar to this one: ┌──── │ (defun
jao-recoll-format (title url mime-type) │ ;; remove from url the common prefixes
/home/jao/{org/doc,doc,...} │ (let* ((u (replace-regexp-in-string \"/home/jao/\"
\"\" url)) │ 	 (u (replace-regexp-in-string │
\"\\\\(doc\\\\|org/doc\\\\|.emacs.d/gnus/Mail\\\\|var/mail\\\\)/\" \"\" u))) │ (format \"%s
(%s, %s)\" │ 	 (propertize title face consult-recoll-title-face) │ 	 (propertize
u face consult-recoll-url-face) │ 	 (propertize mime-type face
consult-recoll-mime-face)))) │ │ │ (setq consult-recoll-format-candidate
#'jao-recoll-format) └──── 3.2 Integration with embark-collect
─────────────────────────────────── If you use [embark], you can use
`embark-collect to export the list of search results in the minibuffer to an
/Embark collect/ buffer.  To allow opening buffer in that buffer as if they had
been selected in the minibuffer, enable integration with embark adding this call
to your init file: ┌──── │ (consult-recoll-embark-setup) └──── [embark]
<http://elpa.gnu.org/packages/embark.html> 3.3 Tip: displaying snippets in
results list ──────────────────────────────────────────── Instead of relying on
a separate preview buffer to display snippets, you can set
`consult-recoll-inline-snippets to `t to show them in the minibuffer, as
individual candidates.
<https://codeberg.org/jao/consult-recoll/raw/branch/main/consult-recoll-inline.png>
3.4 Tip: disabling mime type groups ─────────────────────────────────── By
default, results are listed grouped by their mime type.  You can disable
grouping by setting the customizable variable `consult-recoll-group-by-mime to
`nil'.
<https://codeberg.org/jao/consult-recoll/raw/branch/main/consult-recoll-no-groups.png>
4 Opening search results ════════════════════════ When a search result candidate
is selected, its MIME type is used to look up a function to open its associated
file in the customizable variable `consult-recoll-open-fns'.  If no entry is
found, consult-recoll uses the value of `consult-open-fn as a default.  If the
latter is not set, `eww-open-file is used for HTML files and `find-file for the
rest, moving to the result's page number if the major mode of the opened file is
either `doc-view-mode or `pdf-view-mode'.  If `consult-recoll-inline-snippets is
set, the functions above take two arguments: the URL of the file to open and, if
present, the snippet page number (or `nil if it is not available, e.g., because
the selected candidate is the one showing the document data).  If the selected
candidate is a snippet corresponding to a text MIME and the page number of the
snippet is 0 (as is often the case, since text files are normally not
paginated), `consult-recoll will perform a search for the snippet text after
opening the file.  See also [Integration with embark-collect] for an alternative
way of listing and opening search results using embark. [Integration with
embark-collect] See section 3.2 4.1 Example: opening PDFs with external viewer
────────────────────────────────────────────── For instance, if you want to use
`zathura to open PDF documents, you could define an elisp helper like: ┌──── │
(defun open-with-zathura (file &optional page) │ (shell-command (format \"zathura
%s -P %s\" file (or page 1)))) └──── and then add it to
`consult-recoll-open-fns': ┌──── │ (add-to-list consult-recoll-open-fns
(\"application/pdf\" .  open-with-zathura)) └──── 4.2 Example: Opening emails with
notmuch ──────────────────────────────────────── If you use [notmuch] and
include your maildirs in recoll's indexed directories, a simple way to open a
candidate result given its file name is to find out the message's ID and use
`notmuch.el''s function `notmuch-show to open it: ┌──── │ (defun
open-with-notmuch (file &optional _page) │ (with-temp-buffer │
(insert-file-contents-literally file) │ (goto-char (point-min)) │ (and
(re-search-forward \"^Message-ID: <\\\\([^>]+\\\\)>$\" nil t) │ 	 (notmuch-show
(concat \"id:\" (match-string 1)))))) │ │ (add-to-list consult-recoll-open-fns
(\"message/rfc822\" .  open-with-notmuch)) └──── [notmuch]
<https://notmuchmail.org/> 5 Thanks ════════ Thanks to • [Nicholas P. Rougier]
for useful discussions and suggestions, including actual fixes. • [Stefan
Monnier] for setting up the GNU ELPA package. • Johan Widén for tips on using
consult-recoll with helm. [Nicholas P. Rougier] <https://codeberg.org/rougier>
[Stefan Monnier] <https://codeberg.org/monnier>")
    (license license:gpl3+)))

(define-public emacs-consult-yasnippet
  (package
    (name "emacs-consult-yasnippet")
    (version "20220724.1338")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mohkale/consult-yasnippet.git")
                    (commit "ae0450889484f23dc4ec37518852a2c61b89f184")))
              (sha256
               (base32
                "13hmmsnmh32vafws61sckzzy354rq0nslqpyzhw97iwvn0fpsa35"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-yasnippet emacs-consult))
    (home-page "https://github.com/mohkale/consult-yasnippet")
    (synopsis "A consulting-read interface for yasnippet")
    (description
     "Interactively select a yasnippet snippet through completing-read with in buffer
previews.")
    (license #f)))


;;; Prot's packages

(define-public emacs-fontaine
  (package
   (name "emacs-fontaine")
   (version "0.4.1")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://elpa.gnu.org/packages/fontaine-"
                                version ".tar"))
            (sha256
             (base32
              "0szj9ys7bkj6cwg2bmb7sniyzjzdy3f7qm9r90grrgs5iir3k2qa"))))
   (build-system emacs-build-system)
   (home-page "https://git.sr.ht/~protesilaos/fontaine")
   (synopsis "Set font configurations using presets")
   (description
    "	   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 	       FONTAINE.EL: SET
FONT CONFIGURATIONS USING 				PRESETS

			  Protesilaos Stavrou 			  info@protesilaos.com
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


This manual, written by Protesilaos Stavrou, describes the customization options
for `fontaine' (or `fontaine.el'), and provides every other piece of information
pertinent to it.

The documentation furnished herein corresponds to stable version 0.4.0, released
on 2022-09-07.  Any reference to a newer feature which does not yet form part of
the latest tagged commit, is explicitly marked as such.

Current development target is 0.5.0-dev.

⁃ Package name (GNU ELPA): `fontaine' ⁃ Official manual:
<https://protesilaos.com/emacs/fontaine> ⁃ Change log:
<https://protesilaos.com/emacs/fontaine-changelog> ⁃ Git repo on SourceHut:
<https://git.sr.ht/~protesilaos/fontaine>   • Mirrors:     ⁃ GitHub:
<https://github.com/protesilaos/fontaine>     ⁃ GitLab:
<https://gitlab.com/protesilaos/fontaine> ⁃ Mailing list:
<https://lists.sr.ht/~protesilaos/fontaine>

Table of Contents ─────────────────

1.  COPYING 2.  Overview ..  1.  Shared and implicit fallback values for presets
3.  Installation ..  1.  GNU ELPA package ..  2.  Manual installation 4.  Sample
configuration ..  1.  Persist font configurations on theme switch 5.
Acknowledgements 6.  GNU Free Documentation License 7.  Indices ..  1.  Function
index ..  2.  Variable index ..  3.  Concept index


1 COPYING ═════════

  Copyright (C) 2022 Free Software Foundation, Inc.

        Permission is granted to copy, distribute and/or modify         this
document under the terms of the GNU Free         Documentation License, Version
1.3 or any later version         published by the Free Software Foundation; with
no         Invariant Sections, with the Front-Cover Texts being “A         GNU
Manual,” and with the Back-Cover Texts as in (a)         below.  A copy of the
license is included in the section         entitled “GNU Free Documentation
License.”

        (a) The FSF’s Back-Cover Text is: “You have the freedom to         copy
and modify this GNU manual.”


2 Overview ══════════

  Fontaine lets the user specify presets of font configurations and set   them
on demand on graphical Emacs frames.  The user option   `fontaine-presets' holds
all such presets.

  Presets consist of a list of properties that govern the family,   weight, and
height of the faces `default', `fixed-pitch',   `fixed-pitch-serif',
`variable-pitch', `bold', and `italic'.  Each   preset is identified by a
user-defined symbol as the car of a property   list.  It looks like this (check
the default value of   `fontaine-presets' for how everything is pieced
together):

  ┌────   │ (regular   │  ;; I keep all properties for didactic purposes, but
most can be   │  ;; omitted.    │  :default-family \"Monospace\"   │
:default-weight regular   │  :default-height 100   │  :fixed-pitch-family nil ;
falls back to :default-family   │  :fixed-pitch-weight nil ; falls back to
:default-weight   │  :fixed-pitch-height 1.0   │  :fixed-pitch-serif-family nil
; falls back to :default-family   │  :fixed-pitch-serif-weight nil ; falls back
to :default-weight   │  :fixed-pitch-serif-height 1.0   │
:variable-pitch-family \"Sans\"   │  :variable-pitch-weight nil   │
:variable-pitch-height 1.0   │  :bold-family nil ; use whatever the underlying
face has   │  :bold-weight bold   │  :italic-family nil   │  :italic-slant
italic   │  :line-spacing nil)   └────

  The doc string of `fontaine-presets' explains all properties in detail   and
documents some important caveats or information about font   settings in Emacs.

  [Shared and implicit fallback values for presets].

  The command `fontaine-set-preset' applies the desired preset.  If   there is
only one available, it implements it outright.  Otherwise it   produces a
minibuffer prompt with completion among the available   presets.  When called
from Lisp, the `fontaine-set-preset' requires a   PRESET argument, such as:

  ┌────   │ (fontaine-set-preset 'regular)   └────

  The default behaviour of `fontaine-set-preset' is to change fonts   across all
graphical frames.  The user can, however, limit the changes   to a given frame.
For interactive use, this is done by invoking the   command with a universal
prefix argument (`C-u' by default), which   changes fonts only in the current
frame.  When used in Lisp, the FRAME   argument can be a frame object (satisfies
`framep') or a non-nil   value: the former applies the effects to the given
object, while the   latter means the current frame and thus is the same as
interactively   supplying the prefix argument.

  The command `fontaine-set-face-font' prompts with completion for a   face and
then asks the user to specify the value of the relevant   properties.  Preferred
font families can be defined in the user option   `fontaine-font-families',
otherwise Fontaine will try to find suitable   options among the fonts installed
on the system (not always reliable,   depending on the Emacs build and
environment it runs in).  The list of   faces to choose from is the same as that
implied by the   `fontaine-presets'.  Properties to change and their respective
values   will depend on the face.  For example, the `default' face requires a
natural number for its height attribute, whereas every other face   needs a
floating point (understood as a multiple of the default   height).  This command
is for interactive use only and is supposed to   be used for previewing certain
styles before eventually codifying them   as presets.

  Changing the `bold' and `italic' faces only has a noticeable effect if   the
underlying theme does not hardcode a weight and slant but inherits   from those
faces instead (e.g.  the `modus-themes').

  The `fontaine-set-face-font' also accepts an optional FRAME argument,   which
is the same as what was described above for   `fontaine-set-preset'.

  The latest value of `fontaine-set-preset' is stored in a file whose   location
is defined in `fontaine-latest-state-file' (normally part of   the `.emacs.d'
directory).  Saving is done by the function   `fontaine-store-latest-preset',
which should be assigned to a hook   (e.g. `kill-emacs-hook').  To restore that
value, the user can call   the function `fontaine-restore-latest-preset' (such
as by adding it to   their init file).

  For users of the `no-littering' package, `fontaine-latest-state-file'   is not
stored in their `.emacs.d', but in a standard directory   instead:
<https://github.com/emacscollective/no-littering>.

  As for the name of this package, it is the French word for “fountain”   which,
in turn, is what the font or source is.  However, I will not   blame you if you
can only interpret it as a descriptive acronym: FONTs   Are Irrelevant in
Non-graphical Emacs (because that is actually true).


[Shared and implicit fallback values for presets] See section 2.1

2.1 Shared and implicit fallback values for presets
───────────────────────────────────────────────────

  The user option `fontaine-presets' may look like this (though check   its
default value before you make any edits):

  ┌────   │ (setq fontaine-presets   │       '((regular   │ 	 :default-family
\"Hack\"   │ 	 :default-weight normal   │ 	 :default-height 100   │
:fixed-pitch-family \"Fira Code\"   │ 	 :fixed-pitch-weight nil ; falls back to
:default-weight   │ 	 :fixed-pitch-height 1.0   │ 	 :variable-pitch-family \"Noto
Sans\"   │ 	 :variable-pitch-weight normal   │ 	 :variable-pitch-height 1.0   │
:bold-family nil ; use whatever the underlying face has   │ 	 :bold-weight bold
 │ 	 :italic-family \"Source Code Pro\"   │ 	 :italic-slant italic   │
:line-spacing 1)   │ 	(large   │ 	 :default-family \"Iosevka\"   │
:default-weight normal   │ 	 :default-height 150   │ 	 :fixed-pitch-family nil ;
falls back to :default-family   │ 	 :fixed-pitch-weight nil ; falls back to
:default-weight   │ 	 :fixed-pitch-height 1.0   │ 	 :variable-pitch-family
\"FiraGO\"   │ 	 :variable-pitch-weight normal   │ 	 :variable-pitch-height 1.05
│ 	 :bold-family nil ; use whatever the underlying face has   │ 	 :bold-weight
bold   │ 	 :italic-family nil ; use whatever the underlying face has   │
:italic-slant italic   │ 	 :line-spacing 1)))   └────

  Notice that not all properties need to be specified, as they have   reasonable
fallback values.  The above can be written thus (removed   lines are left empty
for didactic purposes):

  ┌────   │ (setq fontaine-presets   │       '((regular   │ 	 :default-family
\"Hack\"   │    │ 	 :default-height 100   │ 	 :fixed-pitch-family \"Fira Code\"   │
 │    │ 	 :variable-pitch-family \"Noto Sans\"   │    │    │    │    │
:italic-family \"Source Code Pro\"   │    │ 	 :line-spacing 1)   │ 	(large   │
:default-family \"Iosevka\"   │    │ 	 :default-height 150   │    │    │    │
:variable-pitch-family \"FiraGO\"   │    │    │    │    │    │    │
:line-spacing 1)))   └────

  Without the empty lines, we have this, which yields the same results   as the
first example:

  ┌────   │ (setq fontaine-presets   │       '((regular   │ 	 :default-family
\"Hack\"   │ 	 :default-height 100   │ 	 :fixed-pitch-family \"Fira Code\"   │
:variable-pitch-family \"Noto Sans\"   │ 	 :italic-family \"Source Code Pro\"   │
:line-spacing 1)   │ 	(large   │ 	 :default-family \"Iosevka\"   │
:default-height 150   │ 	 :variable-pitch-family \"FiraGO\"   │ 	 :line-spacing
1)))   └────

  We call the properties of the removed lines “implicit fallback   values”.

  This already shows us that the value of `fontaine-presets' does not   need to
be extensive.  To further improve its conciseness, it accepts   a special preset
that provides a list of “shared fallback properties”:   the `t' preset.  This
one is used to define properties that are common   to multiple presets, such as
the `regular' and `large' we have   illustrated thus far.  Here is how verbose
presets can be expressed   succinctly:

  ┌────   │ ;; Notice the duplication of properties and how we will avoid it.
│ (setq fontaine-presets   │       '((regular   │ 	 :default-family \"Iosevka
Comfy\"   │ 	 :default-weight normal   │ 	 :default-height 100   │
:fixed-pitch-family nil ; falls back to :default-family   │
:fixed-pitch-weight nil ; falls back to :default-weight   │
:fixed-pitch-height 1.0   │ 	 :variable-pitch-family \"FiraGO\"   │
:variable-pitch-weight normal   │ 	 :variable-pitch-height 1.05   │
:bold-family nil ; use whatever the underlying face has   │ 	 :bold-weight bold
 │ 	 :italic-family nil   │ 	 :italic-slant italic   │ 	 :line-spacing nil)   │
	(medium   │ 	 :default-family \"Iosevka Comfy\"   │ 	 :default-weight semilight
│ 	 :default-height 140   │ 	 :fixed-pitch-family nil ; falls back to
:default-family   │ 	 :fixed-pitch-weight nil ; falls back to :default-weight
│ 	 :fixed-pitch-height 1.0   │ 	 :variable-pitch-family \"FiraGO\"   │
:variable-pitch-weight normal   │ 	 :variable-pitch-height 1.05   │
:bold-family nil ; use whatever the underlying face has   │ 	 :bold-weight bold
 │ 	 :italic-family nil   │ 	 :italic-slant italic   │ 	 :line-spacing nil)   │
	(large   │ 	 :default-family \"Iosevka Comfy\"   │ 	 :default-weight semilight
│ 	 :default-height 180   │ 	 :fixed-pitch-family nil ; falls back to
:default-family   │ 	 :fixed-pitch-weight nil ; falls back to :default-weight
│ 	 :fixed-pitch-height 1.0   │ 	 :variable-pitch-family \"FiraGO\"   │
:variable-pitch-weight normal   │ 	 :variable-pitch-height 1.05   │
:bold-family nil ; use whatever the underlying face has   │ 	 :bold-weight
extrabold   │ 	 :italic-family nil   │ 	 :italic-slant italic   │
:line-spacing nil)))   │    │ (setq fontaine-presets   │       '((regular   │
:default-height 100)   │ 	(medium   │ 	 :default-weight semilight   │
:default-height 140)   │ 	(large   │ 	 :default-weight semilight   │
:default-height 180   │ 	 :bold-weight extrabold)   │ 	(t ; our shared fallback
properties   │ 	 :default-family \"Iosevka Comfy\"   │ 	 :default-weight normal
│ 	 ;; :default-height 100   │ 	 :fixed-pitch-family nil ; falls back to
:default-family   │ 	 :fixed-pitch-weight nil ; falls back to :default-weight
│ 	 :fixed-pitch-height 1.0   │ 	 :variable-pitch-family \"FiraGO\"   │
:variable-pitch-weight normal   │ 	 :variable-pitch-height 1.05   │
:bold-family nil ; use whatever the underlying face has   │ 	 :bold-weight bold
 │ 	 :italic-family nil   │ 	 :italic-slant italic   │ 	 :line-spacing nil)))
└────

  The `t' preset does not need to explicitly cover all properties.  It   can
rely on the aforementioned “implicit fallback values” to further   reduce its
verbosity (though the user can always write all properties   if they intend to
change their values).  We then have this   transformation:

  ┌────   │ ;; The verbose form   │ (setq fontaine-presets   │       '((regular
 │ 	 :default-height 100)   │ 	(medium   │ 	 :default-weight semilight   │
:default-height 140)   │ 	(large   │ 	 :default-weight semilight   │
:default-height 180   │ 	 :bold-weight extrabold)   │ 	(t ; our shared fallback
properties   │ 	 :default-family \"Iosevka Comfy\"   │ 	 :default-weight normal
│ 	 ;; :default-height 100   │ 	 :fixed-pitch-family nil ; falls back to
:default-family   │ 	 :fixed-pitch-weight nil ; falls back to :default-weight
│ 	 :fixed-pitch-height 1.0   │ 	 :variable-pitch-family \"FiraGO\"   │
:variable-pitch-weight normal   │ 	 :variable-pitch-height 1.05   │
:bold-family nil ; use whatever the underlying face has   │ 	 :bold-weight bold
 │ 	 :italic-family nil   │ 	 :italic-slant italic   │ 	 :line-spacing nil)))
│    │ ;; The concise one which relies on \"implicit fallback values\"   │ (setq
fontaine-presets   │       '((regular   │ 	 :default-height 100)   │ 	(medium
│ 	 :default-weight semilight   │ 	 :default-height 140)   │ 	(large   │
:default-weight semilight   │ 	 :default-height 180   │ 	 :bold-weight
extrabold)   │ 	(t ; our shared fallback properties   │ 	 :default-family
\"Iosevka Comfy\"   │ 	 :default-weight normal   │ 	 :variable-pitch-family
\"FiraGO\"   │ 	 :variable-pitch-height 1.05)))   └────


3 Installation ══════════════




3.1 GNU ELPA package ────────────────────

  The package is available as `fontaine'.  Simply do:

  ┌────   │ M-x package-refresh-contents   │ M-x package-install   └────


  And search for it.

  GNU ELPA provides the latest stable release.  Those who prefer to   follow the
development process in order to report bugs or suggest   changes, can use the
version of the package from the GNU-devel ELPA   archive.  Read:
<https://protesilaos.com/codelog/2022-05-13-emacs-elpa-devel/>.


3.2 Manual installation ───────────────────────

  Assuming your Emacs files are found in `~/.emacs.d/', execute the   following
commands in a shell prompt:

  ┌────   │ cd ~/.emacs.d   │    │ # Create a directory for manually-installed
packages   │ mkdir manual-packages   │    │ # Go to the new directory   │ cd
manual-packages   │    │ # Clone this repo, naming it \"fontaine\"   │ git clone
https://git.sr.ht/~protesilaos/fontaine fontaine   └────

  Finally, in your `init.el' (or equivalent) evaluate this:

  ┌────   │ ;; Make Elisp files in that directory available to the user.    │
(add-to-list 'load-path \"~/.emacs.d/manual-packages/fontaine\")   └────

  Everything is in place to set up the package.


4 Sample configuration ══════════════════════

  Remember to read the relevant doc strings.

  ┌────   │ (require 'fontaine)   │    │ (setq fontaine-latest-state-file   │
   (locate-user-emacs-file \"fontaine-latest-state.eld\"))   │    │ ;; Iosevka
Comfy is my highly customised build of Iosevka with   │ ;; monospaced and
duospaced (quasi-proportional) variants as well as   │ ;; support or no support
for ligatures:   │ ;; <https://git.sr.ht/~protesilaos/iosevka-comfy>.    │ ;;
│ ;; Iosevka Comfy            == monospaced, supports ligatures   │ ;; Iosevka
Comfy Fixed      == monospaced, no ligatures   │ ;; Iosevka Comfy Duo        ==
quasi-proportional, supports ligatures   │ ;; Iosevka Comfy Wide       == like
Iosevka Comfy, but wider   │ ;; Iosevka Comfy Wide Fixed == like Iosevka Comfy
Fixed, but wider   │ (setq fontaine-presets   │       '((tiny   │
:default-family \"Iosevka Comfy Wide Fixed\"   │ 	 :default-height 70)   │ 	(small
 │ 	 :default-family \"Iosevka Comfy Fixed\"   │ 	 :default-height 90)   │
	(regular   │ 	 :default-height 100)   │ 	(medium   │ 	 :default-height 110)   │
	(large   │ 	 :default-weight semilight   │ 	 :default-height 140   │
:bold-weight extrabold)   │ 	(presentation   │ 	 :default-weight semilight   │
:default-height 170   │ 	 :bold-weight extrabold)   │ 	(jumbo   │
:default-weight semilight   │ 	 :default-height 220   │ 	 :bold-weight
extrabold)   │ 	(t   │ 	 ;; I keep all properties for didactic purposes, but
most can be   │ 	 ;; omitted.  See the fontaine manual for the technicalities:
│ 	 ;; <https://protesilaos.com/emacs/fontaine>.    │ 	 :default-family \"Iosevka
Comfy\"   │ 	 :default-weight regular   │ 	 :default-height 100   │
:fixed-pitch-family nil ; falls back to :default-family   │
:fixed-pitch-weight nil ; falls back to :default-weight   │
:fixed-pitch-height 1.0   │ 	 :fixed-pitch-serif-family nil ; falls back to
:default-family   │ 	 :fixed-pitch-serif-weight nil ; falls back to
:default-weight   │ 	 :fixed-pitch-serif-height 1.0   │ 	 :variable-pitch-family
\"Iosevka Comfy Duo\"   │ 	 :variable-pitch-weight nil   │
:variable-pitch-height 1.0   │ 	 :bold-family nil ; use whatever the underlying
face has   │ 	 :bold-weight bold   │ 	 :italic-family nil   │ 	 :italic-slant
italic   │ 	 :line-spacing nil)))   │    │ ;; Recover last preset or fall back
to desired style from   │ ;; `fontaine-presets'.    │ (fontaine-set-preset (or
(fontaine-restore-latest-preset) 'regular))   │    │ ;; The other side of
`fontaine-restore-latest-preset'.    │ (add-hook 'kill-emacs-hook
#'fontaine-store-latest-preset)   │    │ ;; fontaine does not define any key
bindings.  This is just a sample that   │ ;; respects the key binding
conventions.  Evaluate:   │ ;;   │ ;;     (info \"(elisp) Key Binding
Conventions\")   │ (define-key global-map (kbd \"C-c f\") #'fontaine-set-preset)
│ (define-key global-map (kbd \"C-c F\") #'fontaine-set-face-font)   └────


4.1 Persist font configurations on theme switch
───────────────────────────────────────────────

  Themes re-apply face definitions when they are loaded.  This is   necessary to
render the theme.  For certain faces, such as `bold' and   `italic', it means
that their font family may be reset (depending on   the particularities of the
theme).

  To avoid such a problem, we can arrange to restore the current font   preset
which was applied by `fontaine-set-preset'.  Fontaine provides   the command
`fontaine-apply-current-preset'.  It can either be called   interactively after
loading a theme or be assigned to a hook that is   ran at the post `load-theme'
phase.

  Some themes that provide a hook are the `modus-themes' and `ef-themes'   (both
by Protesilaos), so we can use something like:

  ┌────   │ (add-hook 'modus-themes-after-load-theme-hook
#'fontaine-apply-current-preset))   └────

  If both packages are used, we can either write two lines of `add-hook'   or do
this:

  ┌────   │ ;; Persist font configurations while switching themes (doing it with
 │ ;; my `modus-themes' and `ef-themes' via the hooks they provide).    │
(dolist (hook '(modus-themes-after-load-theme-hook ef-themes-post-load-hook))
│   (add-hook hook #'fontaine-apply-current-preset))   └────

  Themes must specify a hook that is called by their relevant commands   at the
post-theme-load phase.  This can also be done in a   theme-agnostic way:

  ┌────   │ ;; Set up the `after-enable-theme-hook'   │ (defvar
after-enable-theme-hook nil   │   \"Normal hook run after enabling a theme.\")   │
   │ (defun run-after-enable-theme-hook (&rest _args)   │   \"Run
`after-enable-theme-hook'.\"   │   (run-hooks 'after-enable-theme-hook))   │    │
(advice-add 'enable-theme :after #'run-after-enable-theme-hook)   └────

  And then simply use that hook:

  ┌────   │ (add-hook 'after-enable-theme-hook #'fontaine-apply-current-preset)
 └────


5 Acknowledgements ══════════════════

  Fontaine is meant to be a collective effort.  Every bit of help   matters.

  Author/maintainer         Protesilaos Stavrou.

  Contributions to the code or manual         Christopher League, Eli Zaretskii,
Florent Teissier, Terry         F.  Torrey.

  Ideas and user feedback         Joe Higton, Ted Reed.


6 GNU Free Documentation License ════════════════════════════════


7 Indices ═════════

7.1 Function index ──────────────────


7.2 Variable index ──────────────────


7.3 Concept index ─────────────────")
   (license license:gpl3+)))





;;; persistent-scratch

(define-public emacs-persistent-scratch
  (package
    (name "emacs-persistent-scratch")
    (version "20220620.408")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Fanael/persistent-scratch.git")
                    (commit "92f540e7d310ec2e0b636eff1033cf78f0d9eb40")))
              (sha256
               (base32
                "1hl4xac1zsvpbibahp54phf1b1srhnm2nh30vzmh96aynnf38vqd"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/Fanael/persistent-scratch")
    (synopsis "Preserve the scratch buffer across Emacs sessions")
    (description
     "Preserve the state of scratch buffers across Emacs sessions by saving the state
to and restoring it from a file, with autosaving and backups.  Save scratch
buffers: `persistent-scratch-save and `persistent-scratch-save-to-file'.
Restore saved state: `persistent-scratch-restore and
`persistent-scratch-restore-from-file'.  To control where the state is saved,
set `persistent-scratch-save-file'.  What exactly is saved is determined by
`persistent-scratch-what-to-save'.  What buffers are considered scratch buffers
is determined by `persistent-scratch-scratch-buffer-p-function'.  By default,
only the `*scratch* buffer is a scratch buffer.  Autosave can be enabled by
turning `persistent-scratch-autosave-mode on.  Backups of old saved states are
off by default, set `persistent-scratch-backup-directory to a directory to
enable them.  To both enable autosave and restore the last saved state on Emacs
start, add (persistent-scratch-setup-default) to the init file.  This will NOT
error when the save file doesn't exist.  To just restore on Emacs start, it's a
good idea to call `persistent-scratch-restore inside an `ignore-errors or
`with-demoted-errors block.")
    (license #f)))


;;; smartscan

(define-public emacs-smartscan
  (package
    (name "emacs-smartscan")
    (version "20170211.2033")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mickeynp/smart-scan.git")
                    (commit "234e077145710a174c20742de792b97ed2f965f6")))
              (sha256
               (base32
                "1nzkgfr1w30yi88h4kwgiwq4lcd0fpm1cd50gy0csjcpbnyq6ykf"))))
    (build-system emacs-build-system)
    (home-page "unspecified")
    (synopsis "Jumps between other symbols found at point")
    (description
     "This code comes from my article \"Effective Editing I: Movement\"
http://www.masteringemacs.org/articles/2011/01/14/effective-editing-movement/

Smart Scan lets you jump between symbols in your buffer, based on the initial
symbol your point was on when you started the search.  Incremental calls will
still respect the original search query so you can move up or down in your
buffer, quickly, to find other matches without having to resort to `isearch' to
find things first.  The main advantage over isearch is speed: Smart Scan will
guess the symbol point is on and immediately find other symbols matching it, in
an unintrusive way.

INSTALLATION

Install package (package-install 'smartscan)

Enable minor mode in a specific mode hook using (smartscan-mode 1) or globally
using (global-smartscan-mode 1)

HOW TO USE IT

Simply type `smartscan-symbol-go-forward' (or press M-n) to go forward; or
`smartscan-symbol-go-backward' (M-p) to go back.  Additionally, you can replace
all symbols point is on by invoking M-' (that's \"M-quote\")

CUSTOMIZATION You can customize `smartscan-use-extended-syntax' to alter
(temporarily, when you search) the syntax table used by Smart Scan to find
matches in your buffer.

CHANGES

0.3: Only consider `-' to be part of a word in LISP modes.")
    (license #f)))


;;; dired single

(define-public emacs-dired-single
  (package
    (name "emacs-dired-single")
    (version "20220917.625")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/crocket/dired-single.git")
                    (commit "3bb53664ccdfb2f911667947be6b6c022e4ec758")))
              (sha256
               (base32
                "0xd61mr2h0p1hmid8wq0l80di3mglgvacl01zgsdw2jn93vwmj0b"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/crocket/dired-single")
    (synopsis "Reuse the current dired buffer to visit a directory")
    (description "No description available.")
    (license #f)))


;;; tabspaces
(define-public emacs-tabspaces
  (package
    (name "emacs-tabspaces")
    (version "20220924.1805")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mclear-tools/tabspaces.git")
                    (commit "150c9dd740a2482c66b1e2687f19f6c46504efd2")))
              (sha256
               (base32
                "0y59jm4ji6j1d4rrjjr8ajhym1xmlc7pb93fpmdfwqr06p11zndv"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-project))
    (home-page "https://github.com/mclear-tools/tabspaces")
    (synopsis "Leverage tab-bar and project for buffer-isolated workspaces")
    (description
     "This package provides several functions to facilitate a single frame-based
workflow with one workspace per tab, integration with project.el (for
project-based workspaces) and buffer isolation per tab (i.e.  a \"tabspace\"
workspace).  The package assumes project.el and tab-bar.el are both present
(they are built-in to Emacs 27.1+).  This file is not part of GNU Emacs. ;
Acknowledgements Much of the package code is inspired by: -
https://github.com/kaz-yos/emacs -
https://github.com/wamei/elscreen-separate-buffer-list/issues/8 -
https://www.rousette.org.uk/archives/using-the-tab-bar-in-emacs/ -
https://github.com/minad/consult#multiple-sources -
https://github.com/florommel/bufferlo")
    (license #f)))


;;; 0x0
(define-public emacs-0x0
  (package
   (name "emacs-0x0")
   (version "20210701.839")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/willvaughn/emacs-0x0.git")
                  (commit "63cd5eccc85e527f28e1acc89502a53245000428")))
            (sha256
             (base32
              "1cd0drlhi0lf1vmarcfl3vc7ldkymaj50dhqb1ajm7r0s5ps3asb"))))
   (build-system emacs-build-system)
   (home-page "https://gitlab.com/willvaughn/emacs-0x0")
   (synopsis "Upload sharing to 0x0.st")
   (description
    " Integration with https://0x0.st, envs.sh, ttm.sh, and self-hosted services

Upload whatever you need to a pastebin server.  The commands
`0x0-upload-file',`0x0-upload-text' and `0x0-upload-kill-ring', which
respectively upload (parts of) the current buffer, a file on your disk and a
string from the minibuffer can be used too.

The default host and the host this package is named after is https://0x0.st.
Consider donating to https://liberapay.com/mia/donate if you like the service.")
   (license #f)))


(define-public emacs-sqlformat
  (package
    (name "emacs-sqlformat")
    (version "20210305.209")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/purcell/sqlformat.git")
                    (commit "3fa86085de8a4e70954d4b3346fb228016b5bbb9")))
              (sha256
               (base32
                "1b696mnd07fpspgrmxwyjy5wx8swcl1diiz891ih38jpf7b7x8rf"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-reformatter))
    (home-page "https://github.com/purcell/sqlformat")
    (synopsis "Reformat SQL using sqlformat or pgformatter")
    (description
     "This package provides commands and a minor mode for easily reformatting SQL
using external programs such as \"sqlformat\" and \"pg_format\".  Install the
\"sqlparse\" (Python) package to get \"sqlformat\", or \"pgformatter\" to get
\"pg_format\", or \"sqlfluff\" (Python) to get \"sqlfluff\", Customise the
`sqlformat-command variable as desired.  For example, to use \"pgformatter\"
(i.e., the `pg_format` command) with two-character indent and no statement
grouping, (setq sqlformat-command pgformatter) (setq sqlformat-args (\"-s2\"
\"-g\")) Then call `sqlformat', `sqlformat-buffer or `sqlformat-region as
convenient.  Enable `sqlformat-on-save-mode in SQL buffers like this: (add-hook
sql-mode-hook sqlformat-on-save-mode) or locally to your project with a form in
your .dir-locals.el like this: ((sql-mode (mode .  sqlformat-on-save))) You
might like to bind `sqlformat or `sqlformat-buffer to a key, e.g. with:
(define-key sql-mode-map (kbd \"C-c C-f\") sqlformat)")
    (license #f)))

(define-public emacs-dogears
  (package
    (name "emacs-dogears")
    (version "20220829.441")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/alphapapa/dogears.el.git")
                    (commit "5b8a85d03ca17d8b8185868fdbacf320784026d5")))
              (sha256
               (base32
                "0h4gh4ja9dnslj286skc8nzp9dvpyp53ig9y4kniq5lji6gn3r1f"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-map))
    (arguments
     '(#:include '("^[^/]+.el$" "^[^/]+.el.in$"
                   "^dir$"
                   "^[^/]+.info$"
                   "^[^/]+.texi$"
                   "^[^/]+.texinfo$"
                   "^doc/dir$"
                   "^doc/[^/]+.info$"
                   "^doc/[^/]+.texi$"
                   "^doc/[^/]+.texinfo$")
       #:exclude '("^.dir-locals.el$" "^test.el$" "^tests.el$" "^[^/]+-test.el$"
                   "^[^/]+-tests.el$" "^helm-dogears.el$")))
    (home-page "https://github.com/alphapapa/dogears.el")
    (synopsis "Never lose your place again")
    (description
     "This library automatically and smartly remembers where you've been, in and
across buffers, and helps you quickly return to any of those places.  It uses
the Emacs bookmarks system internally (but without modifying the
bookmarks-alist) to save and restore places with mode-specific functionality.")
    (license #f)))


(define-public emacs-hl-indent-scope
  (package
    (name "emacs-hl-indent-scope")
    (version "20221016.508")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://melpa.org/packages/hl-indent-scope-"
                                  version ".tar"))
              (sha256
               (base32
                "1wmvd0z8ixpsfdrmn8q4slbi83apnfrq0r2rqxp7vd3zl9m8r1ha"))))
    (build-system emacs-build-system)
    (home-page "https://codeberg.org/ideasman42/emacs-hl-indent-scope")
    (synopsis "Highlight indentation by scope")
    (description
     "Highlight indentation by syntax (or user configurable methods).  Currently this
works for C-like and Lisp-like languages, with special support for C/C++ &
CMake.  Tabs are currently not supported. ; Usage (hl-indent-scope-mode) ;;
activate in the current buffer.")
    (license #f)))
