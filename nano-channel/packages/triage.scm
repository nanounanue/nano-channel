(define-module (nano-channel packages triage)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages texinfo))

;; (define-public python-millify
;;   (package
;;    (name "python-millify")
;;    (version "0.1.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "millify" version))
;;      (sha256
;;       (base32 "06kzb6349scv57x6yi6h9kvf1847pzsamr2w3fa3zlmnk3ffz7b1"))))
;;    (build-system python-build-system)
;;    (home-page "https://github.com/azaitsev/millify")
;;    (synopsis "Convert long numbers into a human-readable format in Python")
;;    (description "Convert long numbers into a human-readable format in Python")
;;    (license #f)))

;; (define-public python-vega-datasets
;;   (package
;;    (name "python-vega-datasets")
;;    (version "0.9.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "vega-datasets" version))
;;      (sha256
;;       (base32 "1h1zv607mars2j73v8fdwihjh479blqxyw29nhmc73lf40s9iglx"))))
;;    (build-system python-build-system)
;;    (propagated-inputs `(("python-pandas" ,python-pandas)))
;;    (home-page "http://github.com/altair-viz/vega_datasets")
;;    (synopsis "A Python package for offline access to Vega datasets")
;;    (description "A Python package for offline access to Vega datasets")
;;    (license license:expat)))

;; (define-public python-altair
;;   (package
;;    (name "python-altair")
;;    (version "4.1.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "altair" version))
;;      (sha256
;;       (base32 "0c99q5dy6f275yg1f137ird08wmwc1z8wmvjickkf2mvyka31p9y"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-entrypoints" ,python-entrypoints)
;;       ("python-jinja2" ,python-jinja2)
;;       ("python-jsonschema" ,python-jsonschema)
;;       ("python-numpy" ,python-numpy)
;;       ("python-pandas" ,python-pandas)
;;       ("python-toolz" ,python-toolz)))
;;    (native-inputs
;;     `(("python-black" ,python-black)
;;       ("python-docutils" ,python-docutils)
;;       ("python-flake8" ,python-flake8)
;;       ("python-ipython" ,python-ipython)
;;       ("python-m2r" ,python-m2r)
;;       ("python-pytest" ,python-pytest)
;;       ("python-recommonmark" ,python-recommonmark)
;;       ("python-sphinx" ,python-sphinx)
;;       ("python-vega-datasets" ,python-vega-datasets)))
;;    (home-page "http://altair-viz.github.io")
;;    (synopsis
;;     "Altair: A declarative statistical visualization library for Python.")
;;    (description
;;     "Altair: A declarative statistical visualization library for Python.")
;;    (license #f)))

;; (define-public python-arabic-reshaper
;;   (package
;;    (name "python-arabic-reshaper")
;;    (version "2.1.3")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "arabic-reshaper" version))
;;      (sha256
;;       (base32 "140qrsjfp96p9s4qcb9apghr89p44asn5aawdb662anykmpgqdm2"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-configparser" ,python-configparser)
;;       ("python-future" ,python-future)
;;       ("python-setuptools" ,python-setuptools)))
;;    (home-page "https://github.com/mpcabd/python-arabic-reshaper/")
;;    (synopsis
;;     "Reconstruct Arabic sentences to be used in applications that don't support Arabic")
;;    (description
;;     "Reconstruct Arabic sentences to be used in applications that don't support Arabic")
;;    (license license:expat)))

;; (define-public python-bidi
;;   (package
;;    (name "python-bidi")
;;    (version "0.4.2")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "python-bidi" version))
;;      (sha256
;;       (base32 "0vj93h930v65najq3jkpcy6vlfgy5gnrvw2pqrnrgsdkh8ggfisk"))))
;;    (build-system python-build-system)
;;    (propagated-inputs `(("python-six" ,python-six)))
;;    (home-page "https://github.com/MeirKriheli/python-bidi")
;;    (synopsis "Pure python implementation of the BiDi layout algorithm")
;;    (description "Pure python implementation of the BiDi layout algorithm")
;;    (license #f)))

;; (define-public python-xhtml2pdf
;;   (package
;;    (name "python-xhtml2pdf")
;;    (version "0.2.5")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "xhtml2pdf" version))
;;      (sha256
;;       (base32 "08clndvgqcn1pa15iakyfzl4zjjn4yd57h97x7xhwvy6z9sfk5v7"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-arabic-reshaper" ,python-arabic-reshaper)
;;       ("python-bidi" ,python-bidi)
;;       ("python-html5lib" ,python-html5lib)
;;       ("python-pillow" ,python-pillow)
;;       ("python-pypdf2" ,python-pypdf2)
;;       ("python-reportlab" ,python-reportlab)
;;       ("python-six" ,python-six)))
;;    (home-page "http://github.com/xhtml2pdf/xhtml2pdf")
;;    (synopsis "PDF generator using HTML and CSS")
;;    (description "PDF generator using HTML and CSS")
;;    (license #f)))

;; (define-public python-markdown2
;;   (package
;;    (name "python-markdown2")
;;    (version "2.4.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "markdown2" version))
;;      (sha256
;;       (base32 "0gz1pd5hdxjmfw0zgl76cqg8kdzdyc1lnskv9s9hfklw2z7nb4nf"))))
;;    (build-system python-build-system)
;;    (home-page "https://github.com/trentm/python-markdown2")
;;    (synopsis "A fast and complete Python implementation of Markdown")
;;    (description "A fast and complete Python implementation of Markdown")
;;    (license license:expat)))

;; (define-public python-visitor
;;   (package
;;    (name "python-visitor")
;;    (version "0.1.3")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "visitor" version))
;;      (sha256
;;       (base32 "02j87v93c50gz68gbgclmbqjcwcr7g7zgvk7c6y4x1mnn81pjwrc"))))
;;    (build-system python-build-system)
;;    (home-page "http://github.com/mbr/visitor")
;;    (synopsis "A tiny pythonic visitor implementation.")
;;    (description "A tiny pythonic visitor implementation.")
;;    (license license:expat)))

;; (define-public python-dominate
;;   (package
;;    (name "python-dominate")
;;    (version "2.6.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "dominate" version))
;;      (sha256
;;       (base32 "1r71ny73ws0zf5mcml0x5yfbjhzfkn5id670zv26y2kh4gg2rv3n"))))
;;    (build-system python-build-system)
;;    (home-page "https://github.com/Knio/dominate/")
;;    (synopsis
;;     "Dominate is a Python library for creating and manipulating HTML documents using an elegant DOM API.")
;;    (description
;;     "Dominate is a Python library for creating and manipulating HTML documents using an elegant DOM API.")
;;    (license #f)))

;; (define-public python-flask-bootstrap
;;   (package
;;    (name "python-flask-bootstrap")
;;    (version "3.3.7.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "Flask-Bootstrap" version))
;;      (sha256
;;       (base32 "1j1s2bplaifsnmr8vfxa3czca4rz78xyhrg4chx39xl306afs26b"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-dominate" ,python-dominate)
;;       ("python-flask" ,python-flask)
;;       ("python-visitor" ,python-visitor)))
;;    (home-page "http://github.com/mbr/flask-bootstrap")
;;    (synopsis
;;     "An extension that includes Bootstrap in your project, without any boilerplate code.")
;;    (description
;;     "An extension that includes Bootstrap in your project, without any boilerplate code.")
;;    (license license:bsd-3)))

;; (define-public python-aequitas
;;   (package
;;    (name "python-aequitas")
;;    (version "0.42.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "aequitas" version))
;;      (sha256
;;       (base32 "10brv8fmh8bvdg0nfv74q44wld9cs05q1g0h8mw2sj8f39fpmv8g"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-altair" ,python-altair)
;;       ("python-flask" ,python-flask)
;;       ("python-flask-bootstrap" ,python-flask-bootstrap)
;;       ("python-markdown2" ,python-markdown2)
;;       ("python-matplotlib" ,python-matplotlib)
;;       ("python-millify" ,python-millify)
;;       ("python-ohio" ,python-ohio)
;;       ("python-pandas" ,python-pandas)
;;       ("python-pyyaml" ,python-pyyaml)
;;       ("python-seaborn" ,python-seaborn)
;;       ("python-sqlalchemy" ,python-sqlalchemy)
;;       ("python-tabulate" ,python-tabulate)
;;       ("python-xhtml2pdf" ,python-xhtml2pdf)))
;;    (home-page "https://github.com/dssg/aequitas")
;;    (synopsis "The bias and fairness audit toolkit.")
;;    (description "The bias and fairness audit toolkit.")
;;    (license #f)))

(define-public python-ohio
  (package
   (name "python-ohio")
   (version "0.5.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "ohio" version))
     (sha256
      (base32 "02132br4sbj0b9rfhmkyq0ac94npprzbz812cgr2rgrq54avslpl"))))
   (build-system python-build-system)
   (home-page "https://github.com/dssg/ohio")
   (synopsis "I/O extras")
   (description "I/O extras")
   (license #f)))

;; (define-public python-adjusttext
;;   (package
;;    (name "python-adjusttext")
;;    (version "0.7.3")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "adjustText" version))
;;      (sha256
;;       (base32 "01jbcsx5n4p2r6rs0in3kg07fr6np0a7k5n7pb5q1ndljmd2f3mr"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-matplotlib" ,python-matplotlib)
;;       ("python-numpy" ,python-numpy)))
;;    (home-page "https://github.com/Phlya/adjustText")
;;    (synopsis
;;     "Iteratively adjust text position in matplotlib plots to minimize overlaps")
;;    (description
;;     "Iteratively adjust text position in matplotlib plots to minimize overlaps")
;;    (license #f)))

;; (define-public python-argcmdr
;;   (package
;;    (name "python-argcmdr")
;;    (version "0.7.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "argcmdr" version))
;;      (sha256
;;       (base32 "0vpkzhicq49h2abqlhvnyijw82d2q6jifsxq07byrqhqmgbwad5p"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-argcomplete" ,python-argcomplete)
;;       ("python-dickens" ,python-dickens)
;;       ("python-plumbum" ,python-plumbum)))
;;    (home-page "https://github.com/dssg/argcmdr")
;;    (synopsis
;;     "Thin argparse wrapper for quick, clear and easy declaration of hierarchical console command interfaces")
;;    (description
;;     "Thin argparse wrapper for quick, clear and easy declaration of hierarchical console command interfaces")
;;    (license license:expat)))

;; (define-public python-aioitertools
;;   (package
;;    (name "python-aioitertools")
;;    (version "0.8.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "aioitertools" version))
;;      (sha256
;;       (base32 "0iibihfjqknrmfawr49ncvc7w9nkycis4jcrfdkih3wvpk7zl0lb"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-typing-extensions" ,python-typing-extensions)))
;;    (home-page "https://aioitertools.omnilib.dev")
;;    (synopsis "itertools and builtins for AsyncIO and mixed iterables")
;;    (description "itertools and builtins for AsyncIO and mixed iterables")
;;    (license #f)))

;; (define-public python-aiobotocore
;;   (package
;;    (name "python-aiobotocore")
;;    (version "2.0.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "aiobotocore" version))
;;      (sha256
;;       (base32 "12y4n13xn4bvx302z13vl7gixvnkid37vhlan6whqjbi2sbd6cgh"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-aiohttp" ,python-aiohttp)
;;       ("python-aioitertools" ,python-aioitertools)
;;       ("python-botocore" ,python-botocore)
;;       ("python-wrapt" ,python-wrapt)))
;;    (home-page "https://github.com/aio-libs/aiobotocore")
;;    (synopsis "Async client for aws services using botocore and aiohttp")
;;    (description "Async client for aws services using botocore and aiohttp")
;;    (license #f)))

;; (define-public python-s3fs
;;   (package
;;    (name "python-s3fs")
;;    (version "2021.11.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "s3fs" version))
;;      (sha256
;;       (base32 "1xf9wrpbd71aryarm2x93v6qp7ljfm7v4l3ra2fmqs2vzb0yl8rw"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-aiobotocore" ,python-aiobotocore)
;;       ("python-aiohttp" ,python-aiohttp)
;;       ("python-fsspec" ,python-fsspec)))
;;    (home-page "http://github.com/fsspec/s3fs/")
;;    (synopsis "Convenient Filesystem interface over S3")
;;    (description "Convenient Filesystem interface over S3")
;;    (license license:bsd-3)))

;; (define-public python-signalled-timeout
;;   (package
;;    (name "python-signalled-timeout")
;;    (version "1.0.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "signalled-timeout" version))
;;      (sha256
;;       (base32 "1yc4n3pkivmkpyffgxc5kk9js18s20d1jvpjnlw4r7m6hdq8ma3z"))))
;;    (build-system python-build-system)
;;    (home-page "https://github.com/dssg/signalled-timeout")
;;    (synopsis
;;     "Timeout library for generic interruption of main thread by an exception after a configurable duration")
;;    (description
;;     "Timeout library for generic interruption of main thread by an exception after a configurable duration")
;;    (license #f)))

;; (define-public python-dickens
;;   (package
;;    (name "python-dickens")
;;    (version "1.0.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "Dickens" version))
;;      (sha256
;;       (base32 "0cqly8wagjlw43b72237lx4z3vpiakxzn33faffkdx00cc28h5kj"))))
;;    (build-system python-build-system)
;;    (home-page "https://github.com/dssg/dickens")
;;    (synopsis "Additional decorators implementing the descriptor interface")
;;    (description "Additional decorators implementing the descriptor interface")
;;    (license #f)))

;; (define-public python-sqlalchemy-postgres-copy
;;   (package
;;    (name "python-sqlalchemy-postgres-copy")
;;    (version "0.5.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "sqlalchemy-postgres-copy" version))
;;      (sha256
;;       (base32 "0klmd2yg9w56g6di3l7h9935q0n6y7ag8wmh78hm5m5flskwfzjh"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-psycopg2" ,python-psycopg2)
;;       ("python-six" ,python-six)
;;       ("python-sqlalchemy" ,python-sqlalchemy)))
;;    (home-page "https://github.com/jmcarp/sqlalchemy-postgres-copy")
;;    (synopsis "Utilities for using PostgreSQL COPY with SQLAlchemy")
;;    (description "Utilities for using PostgreSQL COPY with SQLAlchemy")
;;    (license #f)))

;; (define-public python-psycopg2-binary
;;   (package
;;    (name "python-psycopg2-binary")
;;    (version "2.9.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "psycopg2-binary" version))
;;      (sha256
;;       (base32 "0wzp65d1gs6s9ag421vqidk86mvbjacqix31pw708zl3m6jiq8mh"))))
;;    (build-system python-build-system)
;;    (home-page "https://psycopg.org/")
;;    (synopsis "psycopg2 - Python-PostgreSQL Database Adapter")
;;    (description "psycopg2 - Python-PostgreSQL Database Adapter")
;;    (license #f)))

;; (define-public python-triage
;;   (package
;;    (name "python-triage")
;;    (version "5.0.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "triage" version))
;;      (sha256
;;       (base32 "1r1pja3wgv2karnmzz7w7vvm71vv59k53m8b4iw9g2bgbrcr1c3r"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     `(("python-adjusttext" ,python-adjusttext)
;;       ("python-aequitas" ,python-aequitas)
;;       ("python-alembic" ,python-alembic)
;;       ("python-argcmdr" ,python-argcmdr)
;;       ("python-boto3" ,python-boto3)
;;       ("python-click" ,python-click)
;;       ("python-coloredlogs" ,python-coloredlogs)
;;       ("python-dateutil" ,python-dateutil)
;;       ("python-dickens" ,python-dickens)
;;       ("python-graphviz" ,python-graphviz)
;;       ("python-inflection" ,python-inflection)
;;       ("python-matplotlib" ,python-matplotlib)
;;       ("python-numpy" ,python-numpy)
;;       ("python-ohio" ,python-ohio)
;;       ("python-pandas" ,python-pandas)
;;       ("python-pebble" ,python-pebble)
;;       ("python-psycopg2-binary" ,python-psycopg2-binary)
;;       ("python-pyyaml" ,python-pyyaml)
;;       ("python-requests" ,python-requests)
;;       ("python-retrying" ,python-retrying)
;;       ("python-s3fs" ,python-s3fs)
;;       ("python-scikit-learn" ,python-scikit-learn)
;;       ("python-scipy" ,python-scipy)
;;       ("python-seaborn" ,python-seaborn)
;;       ("python-signalled-timeout" ,python-signalled-timeout)
;;       ("python-sqlalchemy" ,python-sqlalchemy)
;;       ("python-sqlalchemy-postgres-copy" ,python-sqlalchemy-postgres-copy)
;;       ("python-sqlparse" ,python-sqlparse)
;;       ("python-verboselogs" ,python-verboselogs)
;;       ("python-wrapt" ,python-wrapt)))
;;    (home-page "https://dssg.github.io/triage/")
;;    (synopsis "Risk modeling and prediction")
;;    (description "Risk modeling and prediction")
;;    (license license:expat)))
